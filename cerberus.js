var http = require("http")

http.createServer((req, res)  => {
  res.write("Hello from cerberus")
  res.end()
}).listen(8080, "0.0.0.0", () => console.log("Cerberus listening on port 8080"))
